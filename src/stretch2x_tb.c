/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch2x SDSoC project
 * 		File: - "stretch2x_tb.cpp"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#include "stretch2x_tb.h"

using namespace std;

class perf_counter
{
public:
     uint64_t tot, cnt, calls;
     perf_counter() : tot(0), cnt(0), calls(0) {};
     inline void reset() { tot = cnt = calls = 0; }
     inline void start() { cnt = sds_clock_counter(); calls++; };
     inline void stop() { tot += (sds_clock_counter() - cnt); };
     inline uint64_t avg_cpu_cycles() { return ((tot+(calls>>1)) / calls); };
};

/*------------------------------- Main --------------------------------------*/
void _p0_I_Stretch2x_HW8_async_8(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW7_async_7(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW6_async_6(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW5_async_5(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW4_async_4(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW3_async_3(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW2_async_2(byte src[256000], byte dest[1228800], byte fragments);
void _p0_I_Stretch2x_HW1_async_1(byte src[256000], byte dest[1228800], byte fragments);
int main(int argc, char** argv )
{
	printf("Starting measure performance with 150 MHZ ... \n");

	perf_counter hw_ctr, sw_ctr;

    /* Errors counter */
    int c_errors = 0;

    int number_values;

    FILE *f_input;

    byte *bytes_img_in;
    byte *bytes_img_out;

    /* Pointers to images*/
    byte *check_out_sw;		// check software output
    byte *check_out_hw; 	// check hardware output

    // printf("Open input.txt file ...");
    char buffer[50];
    snprintf(buffer, 50*sizeof(char), "input_%u_%u.txt", INPUT_WIDTH, INPUT_HEIGHT);
    f_input = fopen(buffer, "r");
    if (f_input==NULL) {fputs ("File error\n",stderr); exit (1);}

    // printf("OK \n");

    // printf("Read data from file ...");
    bytes_img_in = (byte *)malloc(INPUT_WIDTH*INPUT_HEIGHT*sizeof(byte));
    if (bytes_img_in == NULL) {fputs ("Memory error img_in",stderr); exit (2);}

    bytes_img_out = (byte *)malloc(OUTPUT_WIDTH*OUTPUT_HEIGHT*sizeof(byte));
    if (bytes_img_out == NULL) {fputs ("Memory error img_out",stderr); exit (2);}

    number_values = fread (bytes_img_in, 1, INPUT_WIDTH*INPUT_HEIGHT, f_input);
    if (number_values != INPUT_WIDTH*INPUT_HEIGHT) {fputs ("Reading error",stderr); exit (3);}
    fclose(f_input);
    // printf("OK \n");


    /*----------------------------------
    ------------- Hardware -------------
    ----------------------------------*/
    // printf("----------------------\n");
    // printf("Executing Hardware implementation ...\n");

    // printf("Calling the UUT ...");
    hw_ctr.start();

    for (int i=0; i<N_REP; i++) {
_p0_I_Stretch2x_HW1_async_1(bytes_img_in + 0*INPUT_WIDTH*sizeof(byte), bytes_img_out + 0*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW2_async_2(bytes_img_in + 50*INPUT_WIDTH*sizeof(byte), bytes_img_out + 120*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW3_async_3(bytes_img_in + 100*INPUT_WIDTH*sizeof(byte), bytes_img_out + 240*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW4_async_4(bytes_img_in + 150*INPUT_WIDTH*sizeof(byte), bytes_img_out + 360*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW5_async_5(bytes_img_in + 200*INPUT_WIDTH*sizeof(byte), bytes_img_out + 480*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW6_async_6(bytes_img_in + 250*INPUT_WIDTH*sizeof(byte), bytes_img_out + 600*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW7_async_7(bytes_img_in + 300*INPUT_WIDTH*sizeof(byte), bytes_img_out + 720*OUTPUT_WIDTH*sizeof(byte), 8);
_p0_I_Stretch2x_HW8_async_8(bytes_img_in + 350*INPUT_WIDTH*sizeof(byte), bytes_img_out + 840*OUTPUT_WIDTH*sizeof(byte), 8);

    }
    hw_ctr.stop();
    // printf("OK\n");
    

    /*----------------------------------
    ------------- Software -------------
    ----------------------------------*/
    // printf("----------------------\n");
    // printf("Executing Software implementation ...\n");

    /* This is needed to move the destination pointer */
    dest_pitch = PITCH;

    /* Destination has 4 times rows and 4.8 times columns */
    dest_buffer = (byte*)malloc(OUTPUT_WIDTH * OUTPUT_HEIGHT * sizeof(byte));

    /* Transform input in bytes */
    src_buffer = bytes_img_in;

    /* Call software */
    // printf("Calling SW implementation ...");
    sw_ctr.start();
    for (int i=0; i<N_REP; i++) {
		I_Stretch2x_SW(X1, Y1, X2, Y2);
    }
	sw_ctr.stop();
    // printf("OK\n");


    /*-------------------------------------
    --------------- Check -----------------
    -------------------------------------*/
    // printf("----------------------\n");
    // printf("Checking results ...\n");

    /* Take pointers to check values */
    // printf("Gathering images information ...");
    check_out_hw = bytes_img_out;
    check_out_sw = dest_buffer;
    // printf("OK\n");

    /* Loop to check bytes */
    // printf("Checking bytes in both images ... \n");

    for (int i=0; i<(OUTPUT_WIDTH*OUTPUT_HEIGHT); i++)
    {
        /* Check if values are different */
        if (*check_out_sw != *check_out_hw) {

            c_errors++;
            if (i<10) {
            	printf("i: %u\t HW: %u, SW: %u \n",i , *check_out_hw, *check_out_sw);
            }

        } // end if differences

        /* Next values */
        check_out_sw++;
        check_out_hw++;

    } // for I values
    // printf("...\n");
    // printf("OK\n");

    /* Free memory */
    free(dest_buffer);
    free(bytes_img_in);
    free(bytes_img_out);

    // printf("Checking simulation result... \n");
    if (c_errors!=0)
    {
        fprintf(stdout, "-------------------------------------------\n");
        fprintf(stdout, "FAIL\n");
        fprintf(stdout, "Number of errors: %d \n", c_errors);
        fprintf(stdout, "-------------------------------------------\n");
        return 1;
    } // end if no errors
    else
    {
        fprintf(stdout, "TEST PAST\n");
        uint64_t sw_cycles = sw_ctr.avg_cpu_cycles();
        uint64_t hw_cycles = hw_ctr.avg_cpu_cycles();
        double speedup = (double) sw_cycles / (double) hw_cycles;
        std::cout << "Average number of CPU cycles running in software: "
                  << sw_cycles << std::endl;
        std::cout << "Average number of CPU cycles running in hardware: "
                  << hw_cycles << std::endl;
        std::cout << "Modules: " << FRAGMENTS << " N_REP: " << N_REP <<" Speed up: " << speedup << std::endl;
	    fprintf(stdout, "-------------------------------------------\n");
	    return 0;
    } // end if errors

}
