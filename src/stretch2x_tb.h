/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	Stretch2x hardware Accelerator project
 * 		File: - "stretch2x_tb.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 *
 * CHANGELOG:
 *
 *---------------------------------------------------------------------------*/
#ifndef STRETCH2X_TB_H
#define STRETCH2X_TB_H


/*----------------------------- Libraries -----------------------------------*/
#include <stddef.h>
#include <stdio.h>
#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <inttypes.h>
#include "sds_lib.h"

/*---------------------------- Project headers ------------------------------*/
#include "stretch2x_hw.h"
#include "stretch2x_sw.h"


/*---------------------------- Type definitions -----------------------------*/
typedef uint8_t byte;


/*------------------------- Constant Definitions ----------------------------*/
/*
 * Software static arguments, needed in software implementation
 */
#define X1 (0)
#define X2 (640)
#define Y1 (0)
#define Y2 (400)

/*
 * Pitch of destination buffer, ie. screen->pitch
 */
#define PITCH (1280)

/*
 * HW module
 */
#define FRAGMENTS 	(8)

#define INPUT_WIDTH 	(640)
#define INPUT_HEIGHT 	(400)

#define N_REP 			(2000)

#define OUTPUT_WIDTH 	(1280)
#define OUTPUT_HEIGHT 	(960)

#define SCALE_Y 	((float)OUTPUT_WIDTH/INPUT_WIDTH)
#define SCALE_X     (OUTPUT_HEIGHT/INPUT_HEIGHT)

#define CPU_CYCLES

/*------------------------- Variable Definitions ----------------------------*/
/*
 * Global pointers needed in SW implementation
 */
byte *src_buffer, *dest_buffer;
int dest_pitch;


#endif // STRETCH2X_TB_H
